<?php

require('vendor/autoload.php');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

$bot = new \TelegramBot\Api\Client($_ENV['TELEGRAM_BOT_API_TOKEN']);

$bot->command('start', function (\TelegramBot\Api\Types\Message $message) use ($bot) {
    $bot->sendMessage($message->getChat()->getId(), "
        Hello! Send the expression you want to evaluate. 
        /help - available operations and examples
    ");
});

$bot->command('help', function (\TelegramBot\Api\Types\Message $message) use ($bot) {
    $bot->sendMessage($message->getChat()->getId(), "
        Available operations:
        binary operations + - * / % ^, for example: 5 + 5, 2 ^ 10;
        unary operations - sqrt ! sin cos tan, for example: sin 3.14, cos 0;
        brackets, for example (5 + 5) * 2;
        float variables, for example 0.001, 3.14;
        constants pi, e, for example sin(pi)
    ");
});

$bot->on(function (\TelegramBot\Api\Types\Update $update) use ($bot) {
    try {
        $message = $update->getMessage();
        $parser = new \Expression\Parser(new Expression\Tokens\Factory\TokenTypesFactory(), new \Expression\Lexer\Lexer());
        $evaluator = new \Expression\Evaluator();
        $expression = $message->getText();
        $result = $evaluator->execute($parser->parse($expression));
    } catch (\Expression\Exceptions\ParserException $e) {
        $result = $e->getMessage();
    } catch (\Expression\Exceptions\EvaluationException $e) {
        $result = $e->getMessage();
    }

    $bot->sendMessage($message->getChat()->getId(), $result);
}, function () {
    return true;
});

$bot->run();
